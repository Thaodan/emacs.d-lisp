;; https://stackoverflow.com/a/1511827/5745120
(defun which-active-modes ()
  "Which minor modes are enabled in the current buffer."
  (let ((active-modes))
    (mapc (lambda (mode) (condition-case nil
                        (if (and (symbolp mode) (symbol-value mode))
                            (add-to-list 'active-modes mode))
                      (error nil) ))
          minor-mode-list)
    active-modes))

(defun pop-up-frames-switch-to-buffer (buffer alist)
  "Pop up frames switch to buffer command."
  (member 'pop-up-frames-mode (which-active-modes)))

(setq display-buffer-alist
      (append display-buffer-alist
      '((pop-up-frames-switch-to-buffer . ((display-buffer-reuse-window display-buffer-pop-up-frame) . ((reusable-frames . 0)))
))))


(defgroup pop-up-frames nil
  "Highlight Indentation"
  :prefix "pop-up-frames-"
  :group 'basic-faces)

;;;###autoload
(define-minor-mode pop-up-frames-mode
  "Pop up frames minor mode"
  :lighter " PUF")

(defun turn-on-pop-up-frames-mode ()
  "Enable pop-up-frames-mode."
  (interactive)
  (pop-up-frames-mode 1))

(define-globalized-minor-mode global-pop-up-frames-mode
  pop-up-frames-mode turn-on-pop-up-frames-mode)

(provide 'pop-up-frames-mode)
