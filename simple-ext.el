;;; simple-ext.el --- various extensions for simple.el -*- lexical-binding: t -*-
;;
;;
;;; Commentary:
;; Various extensions for simple.el to test before submitting them to Emacs.
;;

;;; Code:
(require 'simple)


(defvar special-keymap-mode-map special-mode-map)

(define-minor-mode special-keymap-mode
  "Special-Keymap mode activates the keymap of `special-mode'."
  :init-value nil
  :keymap special-keymap-mode-map)

(provide 'simple-ext)
;;; simple-ext.el ends here
